FE_Practice By TomWan
==================================
1. I set the study date range and goals of the date range, then I send them with some resource to you;
2. You make planning for every day in the date range;
3. You send your plan to me;
4. I read your plan and give you suggestions;
5. You update your plans followed by my suggestions;
6. Till both of us are agreed with the plan;
7. You excute the plan;
8. You deliver every day's deliverables (from beginning, your deliverable includes your practice codes and your study notes)
9. I review your deliverables;
10. I give your feedbacks about your deliverables every day;
11. You update your practice following my feedbacks;
12. I give you point from 0.00~1.00
13. You promote by yourself;
14. During the date range, to test whether you achieve the goals we set before starting excuting the plan, you'll have to take a few exams designed by me and evaluated by me too. :D

ENJOY YOUR CODING
----------------------------